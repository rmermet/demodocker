var express = require('express');
var app = express();
var os = require('os')
var ip = require("ip");
 
var hostname = process.env.API_HOST | 'localhost';
var port = process.env.API_PORT || 10210;

app.get('/', function(req, res){

    res.send('Hello from: ' +
        ' - Hostname: ' + os.hostname() +
        ' - OS: ' + os.type() + ' - ' + os.platform() +
        ' - Memory: ' + os.totalmem() +
        ' - CPU: ' + os.cpus().length +
        ' - IP: ' + ip.address());
});

var server = app.listen(port, function() {
    console.log('Listening at http://localhost:' + port);
});